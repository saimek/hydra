package pl.incidental.hydra.tests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pl.incidental.hydra.configuration.ApplicationProperties;
import pl.incidental.hydra.driver.manager.DriverManager;
import pl.incidental.hydra.driver.manager.DriverUtils;
import pl.incidental.hydra.page.objects.LoginPage;
import pl.incidental.hydra.page.objects.TopMenuPage;

public class LoginPageTests extends TestBase {

    @Parameters({"username", "token", "warningMessage"})
    @Test
    public void asUserTryToLoginWithIncorrectPassword(String username, String token, String warningMessage) {
        LoginPage loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(username)
                .clickOnNextButton()
                .typeIntoTokenField(token)
                .clickOnNextButton()
                .assertThatWarningIsDisplayed(warningMessage);
    }

    @Parameters({"username", "token", "companyName", "personName"})
    @Test
    public void asUserTryToLoginWithCorrectCredentials(String username, String token, String companyName,
                                                       String personName) {
        LoginPage loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(username)
                .clickOnNextButton()
                .typeIntoTokenField(token)
                .clickOnNextButton();
        TopMenuPage topMenuPage = new TopMenuPage();
        topMenuPage
                .assertThatCompanyNameIsDisplayed(companyName)
                .assertThatPersonNameIsDisplayed(personName)
                .clickOnLogoutButton();
    }

    @Parameters({"username", "token", "warningMessage"})
    @Test
    public void asUserLoginWithCorrectCredentialsButWithWrongTokenType(String username, String token,
                                                                       String warningMessage) {
        LoginPage loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(username)
                .clickOnNextButton()
                .changeTypeOfTokenToTokenFromStaticToken()
                .typeIntoTokenField(token)
                .clickOnNextButton()
                .assertThatWarningIsDisplayed(warningMessage);
    }

    @Parameters({"username", "token", "companyName", "personName"})
    @Test
    public void asUserLoginWithCorrectCredentialsAndTwiceChangingTokenType(String username, String token,
                                                                           String companyName, String personName) {
        LoginPage loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(username)
                .clickOnNextButton()
                .changeTypeOfTokenToTokenFromStaticToken()
                .changeTypeOfTokenToStaticTokenFromToken()
                .typeIntoTokenField(token)
                .clickOnNextButton();
        TopMenuPage topMenuPage = new TopMenuPage();
        topMenuPage
                .assertThatCompanyNameIsDisplayed(companyName)
                .assertThatPersonNameIsDisplayed(personName)
                .clickOnLogoutButton();
    }

    @Parameters({"username", "token", "companyName", "warningMessage" })
    @Test
    public void asLoggedUserTryToLoginInAnotherTab(String username, String token, String companyName, String warningMessage) {
        LoginPage loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(username)
                .clickOnNextButton()
                .typeIntoTokenField(token)
                .clickOnNextButton();
        TopMenuPage topMenuPage = new TopMenuPage();
        topMenuPage
                .assertThatCompanyNameIsDisplayed(companyName);
        DriverManager.openNewTab();
        DriverManager.switchToAnotherTab();
        DriverUtils.navigateToPage(ApplicationProperties.getAppUrl());
        loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(username)
                .clickOnNextButton()
                .assertThatWarningIsDisplayed(warningMessage);
    }

}
