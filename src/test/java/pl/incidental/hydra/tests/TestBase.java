package pl.incidental.hydra.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import pl.incidental.hydra.configuration.ApplicationProperties;
import pl.incidental.hydra.configuration.ConfigurationProperties;
import pl.incidental.hydra.configuration.PropertiesLoader;
import pl.incidental.hydra.driver.manager.DriverManager;
import pl.incidental.hydra.driver.manager.DriverUtils;

public class TestBase {

    @BeforeClass
    public void beforeClass() {
        PropertiesLoader propertiesLoader = new PropertiesLoader();
        ConfigurationProperties.setProperties(propertiesLoader.getPropertiesFromFile("configuration.properties"));
    }

    @BeforeMethod
    public void beforeMethod() {
        DriverUtils.setInitialConfiguration();
        DriverUtils.navigateToPage(ApplicationProperties.getAppUrl());
    }

    @AfterMethod
    public void afterMethod() {
        DriverManager.disposeDriver();
    }
}
