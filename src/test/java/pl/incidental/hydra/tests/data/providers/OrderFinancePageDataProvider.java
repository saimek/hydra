package pl.incidental.hydra.tests.data.providers;

import org.testng.annotations.DataProvider;

public class OrderFinancePageDataProvider {

    @DataProvider(name = "orderFinancePageDataProvider")
    public Object[][] accountListData() {
        Object[][] data = {
                {"46114010100000521969001005", "PLN", "6616.06", null, "2008-02-01", null},
                {"73114010100000521969001004", "PLN", "9570.06", null, "2008-02-01", null},
                {"03114010100000521969001003", "PLN", "8475.44", null, "2008-02-01", null},
                {"30114010100000521969001002", "PLN", "2883.84", null, "2008-02-01", null},
                {"57114010100000521969001001", "PLN", "3527.04", null, "2008-02-01", null}
        };
        return data;
    }
}
