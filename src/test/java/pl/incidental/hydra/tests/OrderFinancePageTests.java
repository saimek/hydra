package pl.incidental.hydra.tests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pl.incidental.hydra.page.objects.LoginPage;
import pl.incidental.hydra.page.objects.TopMenuPage;

public class OrderFinancePageTests {

    @Parameters({"username", "token", "companyName"})
    @Test
    public void asLoggedUserCheckAccountListCorrectness(String username, String token, String companyName,
                                                       String personName) {
        LoginPage loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(username)
                .clickOnNextButton()
                .typeIntoTokenField(token)
                .clickOnNextButton();
        TopMenuPage topMenuPage = new TopMenuPage();
        topMenuPage
                .clickOnOrderFinanceLink()
                .printAccountsDetailsToConsole();//TODO: finish
    }

}
