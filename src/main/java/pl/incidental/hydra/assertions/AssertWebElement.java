package pl.incidental.hydra.assertions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.AbstractAssert;
import org.openqa.selenium.WebElement;

public class AssertWebElement extends AbstractAssert<AssertWebElement, WebElement> {

    private Logger logger = LogManager.getLogger(AssertWebElement.class);

    public AssertWebElement(WebElement element) {
        super(element, AssertWebElement.class);
    }

    public static AssertWebElement assertThat(WebElement webElement){
        return new AssertWebElement(webElement);
    }

    public AssertWebElement isDisplayed() {
        logger.info("checking if element is displayed");
        isNotNull();
        if (!actual.isDisplayed())
            failWithMessage("element is not displayed");
        logger.info("element was displayed");
        return this;
    }

    public AssertWebElement equalsText(String expectedText) {
        logger.info("checking if WebElement has text: " + expectedText);
        isNotNull();
        String actualElementText = actual.getText();
        if (!actualElementText.equals(expectedText))
            failWithMessage("Element text is <%s> expecting to be <%s>", actualElementText, expectedText);
        logger.info("WebElement had expected test");
        return this;
    }

}
