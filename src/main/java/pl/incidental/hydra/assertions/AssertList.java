package pl.incidental.hydra.assertions;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.AbstractAssert;

import java.util.List;

public class AssertList extends AbstractAssert<AssertList, List<?>> {

    private Logger logger = LogManager.getLogger(AssertList.class);
    private List<?> actualList;

    public AssertList(List<?> objects) {
        super(objects, AssertList.class);
    }

    public static AssertList assertThat(List<?> actualList) {
        return new AssertList(actualList);
    }

    public AssertList isEqualTo(List<?> verifiedList) {
        logger.info("checking if lists are equal");
        isNotNull();
        if (!CollectionUtils.isEqualCollection(actualList, verifiedList))
            failWithMessage("lists are not equal");
        logger.info("Lists are equal");
        return this;
    }

}
