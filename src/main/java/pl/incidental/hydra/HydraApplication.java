package pl.incidental.hydra;

import pl.incidental.hydra.configuration.ApplicationProperties;
import pl.incidental.hydra.configuration.ConfigurationProperties;
import pl.incidental.hydra.configuration.PropertiesLoader;
import pl.incidental.hydra.driver.manager.DriverManager;
import pl.incidental.hydra.page.objects.LoginPage;
import pl.incidental.hydra.driver.manager.DriverUtils;
import pl.incidental.hydra.page.objects.TopMenuPage;

public class HydraApplication {

    public static void main(String[] args) {

        PropertiesLoader propertiesLoader = new PropertiesLoader();
        ConfigurationProperties.setProperties(propertiesLoader.
                getPropertiesFromFile("configuration.properties"));

        DriverUtils.setInitialConfiguration();
        DriverUtils.navigateToPage(ApplicationProperties.getAppUrl());

        LoginPage loginPage = new LoginPage();
        loginPage
                .typeIntoIdField(ApplicationProperties.getAppUsername())
                .clickOnNextButton()
                .typeIntoTokenField(ApplicationProperties.getAppPassword())
                .clickOnNextButton();


        TopMenuPage topMenuPage = new TopMenuPage();
        topMenuPage
                .clickOnOrderListLink()
                .clickOnOrderFinanceLink()
                .printAccountsDetailsToConsole()
                .clickOnAccountNoLink("46114010100000521969001005");

        new TopMenuPage().clickOnLogoutButton();


        DriverManager.disposeDriver();

    }

}
