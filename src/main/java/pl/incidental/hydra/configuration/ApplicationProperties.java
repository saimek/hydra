package pl.incidental.hydra.configuration;

public class ApplicationProperties {

    public static String getAppUrl() {
        return ConfigurationProperties.getProperties().getProperty("application.url");
    }

    public static String getAppUsername() {
        return ConfigurationProperties.getProperties().getProperty("application.login");
    }

    public static String getAppPassword() {
        return ConfigurationProperties.getProperties().getProperty("application.password");
    }

}
