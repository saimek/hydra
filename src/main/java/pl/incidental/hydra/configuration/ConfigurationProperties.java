package pl.incidental.hydra.configuration;

import java.util.Properties;

public class ConfigurationProperties {

    private static Properties properties;

    private ConfigurationProperties() {
    }

    public static Properties getProperties() {
        return properties;
    }

    public static void setProperties(Properties properties) {
        if (properties==null)
            throw new IllegalStateException("Please set properties first");
        ConfigurationProperties.properties = properties;
    }
}
