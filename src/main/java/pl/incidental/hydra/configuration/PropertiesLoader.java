package pl.incidental.hydra.configuration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

    public Properties getPropertiesFromFile(String propertiesFileName) {

        InputStream inputStream = null;
        Properties properties = new Properties();

        inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

        try {
            if (inputStream != null)
                properties.load(inputStream);
            else
                throw new FileNotFoundException("Property file " + propertiesFileName + " not found in the classpath");
        } catch (IOException e) {
            RuntimeException ex = new RuntimeException("Cannot load properties due to IOException");
            ex.initCause(e);
            throw ex;
        } finally {
            closeResource(inputStream);
        }
        return properties;
    }

    private void closeResource(InputStream inputStream) {
        try {
            inputStream.close();
        } catch (IOException e) {
            RuntimeException ex = new RuntimeException("Cannot close properties due to IOException");
            ex.initCause(e);
            throw ex;
        }
    }

}
