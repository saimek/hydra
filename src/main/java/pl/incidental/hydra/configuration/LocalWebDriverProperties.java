package pl.incidental.hydra.configuration;

public class LocalWebDriverProperties {

    public static String getChromeWebDriverLocation() {
        return ConfigurationProperties.getProperties().getProperty("chrome.driver.location");
    }
}
