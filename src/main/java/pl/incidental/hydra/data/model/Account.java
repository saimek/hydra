package pl.incidental.hydra.data.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Account {

    private String accountNo;
    private String currencyCode;
    private BigDecimal openingBalance;
    private BigDecimal currentBalance;
    private LocalDate openingBalanceDate;
    private LocalDate currentBalanceDate;

    public Account() {
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo.replace(" ", "");
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        if (openingBalance != null && !openingBalance.equals("")) {
            openingBalance = removeSpacesAndReplaceCommasWithDots(openingBalance);
            this.openingBalance = new BigDecimal(openingBalance);
        }
    }

    private String removeSpacesAndReplaceCommasWithDots(String openingBalance) {
        openingBalance = openingBalance.replace(" ", "").replace(",", ".");
        return openingBalance;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        if (currentBalance != null && !currentBalance.equals("")) {
            currentBalance = removeSpacesAndReplaceCommasWithDots(currentBalance);
            this.currentBalance = new BigDecimal(currentBalance);
        }
    }

    public LocalDate getOpeningBalanceDate() {
        return openingBalanceDate;
    }

    public void setOpeningBalanceDate(String openingBalanceDate) {
        if (openingBalanceDate != null && !openingBalanceDate.equals(""))
            this.openingBalanceDate = LocalDate.parse(openingBalanceDate, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    public LocalDate getCurrentBalanceDate() {
        return currentBalanceDate;
    }

    public void setCurrentBalanceDate(String currentBalanceDate) {
        if (currentBalanceDate != null && !currentBalanceDate.equals("") && !currentBalanceDate.equals("-"))
            this.currentBalanceDate = LocalDate.parse(currentBalanceDate,  DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNo='" + accountNo + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", openingBalance=" + openingBalance +
                ", currentBalance=" + currentBalance +
                ", openingBalanceDate=" + openingBalanceDate +
                ", currentBalanceDate=" + currentBalanceDate +
                '}';
    }
}
