package pl.incidental.hydra.page.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pl.incidental.hydra.assertions.AssertList;
import pl.incidental.hydra.data.model.Account;
import pl.incidental.hydra.driver.manager.DriverManager;
import pl.incidental.hydra.waits.WaitForElement;

import java.util.ArrayList;
import java.util.List;

public class OrderFinancePage {

    @FindBy(css = "tbody[class*='eko-generic-row-group']")
    private List<WebElement> accountListTable;

    @FindBy(id = "button-apply-filter")
    private WebElement refreshButton;

    private By accountNoSelector = new By.ByCssSelector("div[class*='p_accountNo']");

    private List<Account> accountList = new ArrayList<>();

    public OrderFinancePage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }

    private void mapAccountTableToAccountList() {
        for (WebElement accountRow : accountListTable) {
            Account account = new Account();
            account.setAccountNo(accountRow.findElement(accountNoSelector).getText());
            account.setCurrencyCode(accountRow.findElement(By.cssSelector("td[class*='eko-column-currencyCode']")).getText());
            account.setOpeningBalance(accountRow.findElement(By.cssSelector("td[class*='eko-column-openingBalance']")).getText());
            account.setCurrentBalance(accountRow.findElement(By.cssSelector("td[class*='eko-column-currentBalance']")).getText());
            account.setOpeningBalanceDate(accountRow.findElement(By.cssSelector("div[class*='p_openingBalanceDate']")).getText());
            account.setCurrentBalanceDate(accountRow.findElement(By.cssSelector("div[class*='p_currentBalanceDate']")).getText());
            accountList.add(account);
        }
    }

    public OrderFinancePage printAccountsDetailsToConsole() {
        mapAccountTableToAccountList();
        accountList.stream().forEach(x -> System.out.println(x));
        return this;
    }

    public AccountSummaryPage clickOnAccountNoLink(String accountNumber) {
        for (WebElement accountRow : accountListTable) {
            WebElement accountNoLink = accountRow.findElement(accountNoSelector);
            if (accountNoLink.getText().replace(" ", "").equals(accountNumber)) {
                accountNoLink.click();
                break;
            }
        }
        return new AccountSummaryPage();
    }

    public void assertThatAccountListIsConsistent(List<Account> list) {
        WaitForElement.waitUntilElementIsVisible(refreshButton);
        mapAccountTableToAccountList();
        AssertList.assertThat(accountList).isEqualTo(list);
    }

}
