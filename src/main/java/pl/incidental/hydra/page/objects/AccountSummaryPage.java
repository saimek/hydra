package pl.incidental.hydra.page.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pl.incidental.hydra.driver.manager.DriverManager;
import pl.incidental.hydra.waits.WaitForElement;

public class AccountSummaryPage {

    @FindBy(linkText = "Pobierz wyciąg")
    private WebElement getStatementButton;

    public AccountSummaryPage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }

    public void clickOnGetStatementButton() {
        WaitForElement.waitUntilElementIsClickable(getStatementButton);
        getStatementButton.click();
        return;
    }
}
