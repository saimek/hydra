package pl.incidental.hydra.page.objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pl.incidental.hydra.assertions.AssertWebElement;
import pl.incidental.hydra.driver.manager.DriverManager;
import pl.incidental.hydra.waits.WaitForElement;

public class LoginPage {

    private static Logger logger = LogManager.getLogger(LoginPage.class);

    @FindBy(id = "login-id")
    private WebElement idField;

    @FindBy(id = "submit-btn")
    private WebElement nextButton;

    @FindBy(id = "token-field")
    private WebElement tokenField;

    @FindBy(css = "span[class='login-error error-message']")
    private WebElement errorMessage;

    @FindBy(css = "span[class*='drop-arrow']")
    private WebElement tokenTypeSelectionArrow;

    @FindBy(css = "label[class='label-token-1']")
    private WebElement tokenLabel;

    @FindBy(css = "label[class='label-token-2']")
    private WebElement staticTokenLabel;

    public LoginPage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
        DriverManager.getDriver().switchTo().frame("main");
    }

    public LoginPage typeIntoIdField(String username) {
        WaitForElement.waitUntilElementIsVisible(idField);
        idField.sendKeys(username);
        return this;
    }

    public LoginPage clickOnNextButton() {
        WaitForElement.waitUntilElementIsClickable(nextButton);
        nextButton.click();
        return this;
    }

    public LoginPage typeIntoTokenField(String token) {
        WaitForElement.waitUntilElementIsVisible(tokenField);
        tokenField.sendKeys(token);
        return this;
    }

    public LoginPage changeTypeOfTokenToTokenFromStaticToken() {
        changeTypeOfTokenField(tokenLabel);
        return this;
    }

    public LoginPage changeTypeOfTokenToStaticTokenFromToken() {
        changeTypeOfTokenField(staticTokenLabel);
        return this;
    }

    private void changeTypeOfTokenField(WebElement tokenLabel) {
        WaitForElement.waitUntilElementIsClickable(tokenTypeSelectionArrow);
        tokenTypeSelectionArrow.click();
        WaitForElement.waitUntilElementIsClickable(tokenLabel);
        tokenLabel.click();
        WaitForElement.waitUntilElementIsInvisible(tokenLabel);
    }

    public LoginPage assertThatWarningIsDisplayed(String warningMessage) {
        WaitForElement.waitUntilElementIsVisible(errorMessage);
        AssertWebElement.assertThat(errorMessage).equalsText(warningMessage);
        return this;
    }
}

