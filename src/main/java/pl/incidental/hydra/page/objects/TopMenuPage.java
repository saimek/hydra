package pl.incidental.hydra.page.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pl.incidental.hydra.assertions.AssertWebElement;
import pl.incidental.hydra.driver.manager.DriverManager;
import pl.incidental.hydra.waits.WaitForElement;

public class TopMenuPage {

    @FindBy(linkText = "Zlecenia")
    private WebElement orderListLink;

    @FindBy(linkText = "Rachunki i finanse")
    private WebElement orderFinanceLink;

    @FindBy(linkText = "Nowe zlecenie")
    private WebElement newOrderLink;

    @FindBy(css = "span[class='company']")
    private WebElement companyLabel;

    @FindBy(css = "span[class='person']")
    private WebElement personLabel;

    @FindBy(css = "a[class*='icon-power']")
    private WebElement logoutButton;

    public TopMenuPage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }

    public TopMenuPage clickOnOrderListLink() {
        WaitForElement.waitUntilElementIsVisible(orderListLink);
        orderListLink.click();
        return this;
    }

    public OrderFinancePage clickOnOrderFinanceLink() {
        WaitForElement.waitUntilElementIsClickable(orderFinanceLink);
        orderFinanceLink.click();
        return new OrderFinancePage();
    }

    public void clickOnLogoutButton() {
        WaitForElement.waitUntilElementIsClickable(logoutButton);
        logoutButton.click();
    }

    public TopMenuPage assertThatCompanyNameIsDisplayed(String companyName) {
        WaitForElement.waitUntilElementIsVisible(companyLabel);
        AssertWebElement.assertThat(companyLabel).equalsText(companyName);
        return this;
    }

    public TopMenuPage assertThatPersonNameIsDisplayed(String personName) {
        WaitForElement.waitUntilElementIsVisible(personLabel);
        AssertWebElement.assertThat(personLabel).equalsText(personName);
        return this;
    }
}
