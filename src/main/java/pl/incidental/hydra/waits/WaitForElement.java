package pl.incidental.hydra.waits;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pl.incidental.hydra.driver.manager.DriverManager;

public class WaitForElement {

    private static WebDriverWait getWebDriverWait() {
        return new WebDriverWait(DriverManager.getDriver(), 10);
    }

    public static void waitUntilElementIsClickable(WebElement element) {
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitUntilElementIsVisible(WebElement element) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitUntilElementIsStale(WebElement element) {
        getWebDriverWait().until(ExpectedConditions.stalenessOf(element));
    }

    public static void waitUntilElementIsInvisible(WebElement element) {
        getWebDriverWait().until(ExpectedConditions.invisibilityOf(element));
    }
}
