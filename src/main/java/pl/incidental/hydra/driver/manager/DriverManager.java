package pl.incidental.hydra.driver.manager;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pl.incidental.hydra.configuration.LocalWebDriverProperties;
import pl.incidental.hydra.listeners.WebDriverEventListenerRegistrar;

import java.util.Set;

public class DriverManager {

    private static WebDriver driver;

    private DriverManager() {
    }

    public static WebDriver getDriver() {
        if (driver==null) {
            System.setProperty("webdriver.chrome.driver", LocalWebDriverProperties.getChromeWebDriverLocation());
            driver = new ChromeDriver();
            driver = WebDriverEventListenerRegistrar.registerWebDriverEventListener(driver);
        }
        return driver;
    }

    public static void openNewTab() {
        ((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank');");
    }

    public static void switchToAnotherTab() {
        Set<String> handles = driver.getWindowHandles();
        String currentWindowHandle = driver.getWindowHandle();
        for (String handle : handles) {
            if (!currentWindowHandle.equals(handle)) {
                driver.switchTo().window(handle);
            }
        }
    }

    public static void closeDriverWindow(){
        driver.close();
        driver.quit();
    }

    public static void disposeDriver() {
        driver.close();
        driver.quit();
        driver=null;
    }
}
